package CommonUtilityPackage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataReader {

	public static ArrayList<String> readexceldata(String File_name, String Sheet_name, String Test_case_name)
			throws IOException {
		ArrayList<String> arraydata = new ArrayList<String>();
		// step 1 Locate the file
		String projectdir = System.getProperty("user.dir");

		// create obj of file inpurt stream
		FileInputStream fis = new FileInputStream(projectdir + "\\inputdata\\" + File_name);

		// Step2 Access the located excel file
		XSSFWorkbook wb = new XSSFWorkbook(fis);

		// count the no of sheets available in excel file
		int countofsheet = wb.getNumberOfSheets();
		//System.out.println("the total sheets are" + countofsheet);

		// step 4 Access the desired sheet
		for (int i = 0; i < countofsheet; i++) {
			String sheetname = wb.getSheetName(i);
			if (sheetname.equals(Sheet_name)) {
				//System.out.println("inside the sheet" + sheetname);
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> Rows = sheet.iterator();
				while (Rows.hasNext()) {
					Row currentrow = Rows.next();
					// step 5 access the row corresponding to desired test case
					if (currentrow.getCell(0).getStringCellValue().equals(Test_case_name)) {
						Iterator<Cell> Cell = currentrow.iterator();
						while (Cell.hasNext()) {
							String data = Cell.next().getStringCellValue();
							//System.out.println(data);
							arraydata.add(data);
						}
					}

				}
			}

		}
		wb.close();
		return arraydata;

	}

}
