import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import CommonUtilityPackage.ExcelDataReader;

public class DynamicDriver {

	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		// TODO Auto-generated method stub

		// Step 1 read the test cases to be executed from the excel file
		ArrayList<String> testcaseList = ExcelDataReader.readexceldata("api_data.xlsx", "TestCasesToExecute",
				"TestCaseToExecute");
		System.out.println(testcaseList);
		int count = testcaseList.size();

		for (int i = 1; i < count; i++) {

			String TestCaseToExecute = testcaseList.get(i);
			System.out.println("Test Case which is going to execute is:" + TestCaseToExecute);

			// step 2 call the test case to execute on runtime by using java.lang.reflect package
			
			Class<?> TestClass=Class.forName("TestClassPackage."+TestCaseToExecute);
			
			//step 3 call the execute method of the class captured in variable in TestClass by using java.lang.reflect.method
			Method ExecuteMethod=TestClass.getDeclaredMethod("executor");
			
			//set the accessibility of method as true
			ExecuteMethod.setAccessible(true);
			//create the instance of class captured in test class variable
			Object InstanceOfTestClass=TestClass.getDeclaredConstructor().newInstance();
			//execute the method captured in variable  ExecuteMethod inTestClass 
			ExecuteMethod.invoke(InstanceOfTestClass);
			System.out.println("execution of test case name  "+"TestCaseToExecute "+" completed");
			System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		}
	}

}
