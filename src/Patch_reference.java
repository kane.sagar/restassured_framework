import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Patch_reference {

	public static void main(String[] args) {
		String BaseURI="https://reqres.in/";
		String RequestBody="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		RestAssured.baseURI=BaseURI;
		String ResponseBody=given().header("Content-Type","application/json")
				.body(RequestBody).when().patch("api/users/2").then()
				.extract().response().asString();
		System.out.println(ResponseBody);
		
		//creating object of JsonPath class to parse req body
		
		JsonPath jsp_req=new JsonPath(RequestBody);
        String req_name=jsp_req.getString("name");
        System.out.println("this is req Body name:"+req_name);
        
        String req_job=jsp_req.getString("job");
        System.out.println("this is req Body:"+req_job);
        
      //create the obj of JSON path to Parse ResponseBody
        JsonPath jsp_res = new JsonPath(ResponseBody);
        String res_name=jsp_res.getString("name");
        System.out.println("this is res Body:"+res_name);
                
        String res_job=jsp_res.getString("job");
        System.out.println("this is res Body:"+res_job);
        
        Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		

	}

}
