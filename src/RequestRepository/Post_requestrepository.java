package RequestRepository;

import java.io.IOException;
import java.util.ArrayList;
import CommonUtilityPackage.ExcelDataReader;

public class Post_requestrepository extends Endpoints
{
public static String post_tc1_request() throws IOException {
	ArrayList<String> exceldata= ExcelDataReader.readexceldata("api_data.xlsx", "Post_Api", "Post_Tc_2");
	//System.out.println(exceldata);
	String req_name=exceldata.get(1);
	String req_job=exceldata.get(2);
	String requestBody="{\r\n"
			+ "    \"name\": \""+req_name+"\",\r\n"
			+ "    \"job\": \""+req_job+"\"\r\n"
			+ "}";
		System.out.println( requestBody);
	return requestBody;
}
}
