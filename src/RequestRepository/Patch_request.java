package RequestRepository;

import java.io.IOException;
import java.util.ArrayList;

import CommonUtilityPackage.ExcelDataReader;

public class Patch_request extends Endpoints{
	public static String Patch_req() throws IOException {
		ArrayList<String> exceldata= ExcelDataReader.readexceldata("api_data.xlsx", "Patch_Api", "Patch_Tc_1");
		//System.out.println(exceldata);
		String req_name=exceldata.get(1);
		String req_job=exceldata.get(2);
		String requestBody = "{\r\n"
		                                    + "    \"name\": \""+req_name+"\",\r\n"
		                                    + "    \"job\": \""+req_job+"\"\r\n" + "}";
		//System.out.println("the req body is:"+requestBody );
		return requestBody;

	}

}
