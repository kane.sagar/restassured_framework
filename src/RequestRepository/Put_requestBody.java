package RequestRepository;

import java.io.IOException;
import java.util.ArrayList;

import CommonUtilityPackage.ExcelDataReader;

public class Put_requestBody extends Endpoints {

	public static String put_request() throws IOException {
		ArrayList<String> exceldata= ExcelDataReader.readexceldata("api_data.xlsx", "Put_Api", "Put_Tc_2");
		String req_name=exceldata.get(1);
		String req_job=exceldata.get(2);
		String requestBody = "{\r\n"
		                                    + "    \"name\": \""+req_name+"\",\r\n"
		                                    + "    \"job\": \""+req_job+"\"\r\n" + "}";
		System.out.println("the req body is:"+requestBody );
		return requestBody;
	}

}
