import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;
import java.time.LocalDateTime;
import org.testng.Assert;
public class PostReference {

	public static void main(String[] args)
	{
		// step 1: declare the variables for base URI and request BODY
		String BaseURI = "https://reqres.in/";
		String RequestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		// step 2: declare BASE URI
		RestAssured.baseURI = BaseURI;

		// step 3: configure your request body and trigger the API
		String ResponseBody = given().header("Content-Type", "application/json").body(RequestBody).when()
				.post("api/users").then().extract().response().asString();

		//System.out.println(ResponseBody);

		// creating obj of Json Path for Request Body
		JsonPath jsp_req = new JsonPath(RequestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		
		System.out.println("this is req Body:" + req_name);
		System.out.println("this is req Body:" + req_job);

		// create the obj of JSON ptah to Parse ResponseBody
		JsonPath jsp_res = new JsonPath(ResponseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		// System.out.println("this is res Body:" + res_name);
		// System.out.println("this is res Body:" + res_job);

		// parsing ID
		int res_id = jsp_res.getInt("id");
		System.out.println("this is id : " + res_id);

		// parsing createdAt

		String res_createdAt = jsp_res.getString("createdAt");
		String mydate = res_createdAt.substring(0, 10);
		
		//fetching local system date

		LocalDateTime CurrentDate = LocalDateTime.now();
		//System.out.println(CurrentDate);
		String ExpectedDate = CurrentDate.toString().substring(0, 10);

		System.out.println("This is CreatedAT date: " + mydate);
		System.out.println("this is Expected date : " + ExpectedDate);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(ExpectedDate, mydate);
		Assert.assertNotNull(res_id);


		// Assert.assertEquals()

	}

}
