package CommonMethodPackage;

import RequestRepository.Post_requestrepository;
import static io.restassured.RestAssured.given;

public class Triggerapimethod extends Post_requestrepository {

	public static int extract_statuscode_Post(String req_body, String URL) {
		int statuscode = given().header("Content-Type", "application/json").body(req_body).when().post(URL).then()
				.extract().statusCode();
		System.out.println("this is status code"+ statuscode);
		return statuscode;

	}

	public static String extract_body(String req_body, String URL) {

		String ResponseBody = given().header("Content-Type", "application/json").body(req_body).when().post(URL).then()
				.extract().response().asString();
		System.out.println(ResponseBody);
		return ResponseBody;

	}

}
