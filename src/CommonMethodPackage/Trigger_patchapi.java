package CommonMethodPackage;
import RequestRepository.Patch_request;

import static io.restassured.RestAssured.given;

public class Trigger_patchapi extends Patch_request  {
	
	public static int extract_statuscode_Patch(String req_body,String URL){
		
		int statuscode= given().header("Content-Type", "application/json").body(req_body).when().patch(URL).then()
				.extract().statusCode();
		return statuscode;
		
	}
	public static String extract_body(String req_body,String URL) {
		String requestBody=given().header("Content-Type","application/json").body(req_body).when().patch(URL).
				then().extract().response().asString();
		return requestBody;
		
	}

}
