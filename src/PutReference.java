import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
public class PutReference {

	public static void main(String[] args) {
		//step 1: declare variable for baseURI and Request Body
		String BaseURI="https://reqres.in/";
		String ReqBody="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		//step2: declare Base URI
		RestAssured.baseURI=BaseURI;
		
		//step 3: configure req body and trigger the API
		
		String ResponseBody=given().header("Content-Type", "application/json").body(ReqBody).log()
		.all().when().put("api/users/2").then().log().all().extract().response().asString();
		System.out.println(ResponseBody);
		

	}

}
