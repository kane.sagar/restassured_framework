package TestClassPackage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import CommonMethodPackage.Trigger_Putapi;
import CommonUtilityPackage.HandleLogs;
import io.restassured.path.json.JsonPath;

public class Put_tc1 extends Trigger_Putapi {
	public static void executor() throws IOException {
		String requestBody =put_request();
		File dirname = HandleLogs.create_log_dir("Put_tc1");
		//int statuscode = 0;
		for (int i = 0; i < 5; i++) {
			int statuscode =extract_statuscode_Put(requestBody, put_endpoint());
			System.out.println("statuscode is:" + statuscode);
			if (statuscode == 200) {
				String ResponseBody = Trigger_Putapi.extract_body(requestBody, put_endpoint());
				System.out.println("ResponseBody is:" + ResponseBody);
				//HandleLogs.evidence_creator(dirname, "Put_tc1", put_endpoint(), put_request(), ResponseBody);
				validator(requestBody,ResponseBody);
				break;
			} else {
				System.out.println("Desired Status code is not found  please retry");
			}
		}
		//Assert.assertEquals(statuscode, 200);
	}

	public static void validator(String requestBody, String ResponseBody)  throws IOException{
		JsonPath jsp_res = new JsonPath(ResponseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedAt = jsp_res.getString("updatedAt");
		String mydate = res_updatedAt.substring(0, 10);

		// creating for request Body
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 10);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(ExpectedDate, mydate);
	}
}
