import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;

public class GetReference {

	public static void main(String[] args) 
	{
		String BaseURI="https://reqres.in/";
	
		RestAssured.baseURI=BaseURI;
		given().log().all().when().get("api/users/2").then().log().all().extract()
		.response().asString();

	}

}
